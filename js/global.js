"use strict";

let electron = require('electron');

let win = electron.remote.getCurrentWindow();

let fs = require("fs");

var path = require('path');














let hour = 0;
let minute = 0;
let second = 0;

let timer;

let song = [];

let data = {
	playSongMethod: 0,
	fullScreen: false,
	volume: 50
};


const songFolder = './song/';
const dataFile = './data.json';























function ini() {
	readData();

	hideMusicDiv();

	//readData();

	document.getElementById("alarm").innerHTML = "<source src='" + (__dirname.slice(-8) === "app.asar" ? '../../' : './') + "alarm.mp3' type='audio/mpeg'>";



	hourInput.oninput = function () {
		if (this.value.length > 2) {
			this.value = this.value.slice(0,2);
		}
	}
	minuteInput.oninput = function () {
		if (this.value.length > 2) {
			this.value = this.value.slice(0,2);
		}
	}
	secondInput.oninput = function () {
		if (this.value.length > 2) {
			this.value = this.value.slice(0,2);
		}
	}




	let song = document.getElementById("song");
	let songSelect = document.getElementById("songSelect");
	song.onended = function() {
		/*
			data["playSongMethod"]
			0 ==> loop
			1 ==> sequence
			2 ==> random
		*/

		switch (data["playSongMethod"]) {
			case 0:
				//Loop
				songSelect.onchange();
				break;
			case 1:
				//sequence
				if (songSelect.selectedIndex === (songSelect.length - 1)) {
					songSelect.selectedIndex = 1;
				} else {
					songSelect.selectedIndex++;
				}
				songSelect.onchange();
				break;
			case 2:
				//random
				let temp = Math.floor((Math.random() * (songSelect.length - 1)) + 1);
				songSelect.selectedIndex = temp;
				songSelect.onchange();
				break;
		}
	};



	document.getElementById("songInput").addEventListener('change', function() {
		if (this.value.slice(-4) !== ".mp3") {
			document.getElementById("songInputMsg").innerHTML = "<span style='color: red'>Only mp3 file is supported.</span>";
			return;
		}
		document.getElementById("songInputMsg").innerHTML = "";
		fs.copyFile(this.files[0].path, songFolder + this.files[0].name, (err) => {
			if (err) throw err;
			document.getElementById("songInputMsg").innerHTML = "<span style='color: lightgreen'>Added song \"" + this.files[0].name + "\"</span>";
			document.getElementById("songInput").value = "";
		});
	}, false);
}



function toggleScreen() {
	data["fullScreen"] = !data["fullScreen"];
	win.setFullScreen(data["fullScreen"]);
	document.getElementById("fullScreenBt").value = (data["fullScreen"] ? "Exit full screen" : "Full screen");
	saveData();
}



function setTimer() {
	let alarm = document.getElementById("alarm");
	alarm.pause();
	alarm.currentTime = 0;

	hour = document.getElementById("hourInput").value;
	minute = document.getElementById("minuteInput").value;
	second = document.getElementById("secondInput").value;

	if (hour === "") {
		hour = 0;
	}
	if (minute === "") {
		minute = 0;
	}
	if (second === "") {
		second = 0;
	}

	hour = parseInt(hour);
	minute = parseInt(minute);
	second = parseInt(second);

	if (!Number.isInteger(hour)) {
		timerError("Hour is not integer.");
		hour = 0;
		minute = 0;
		second = 0;
		return;
	}
	if (!Number.isInteger(minute)) {
		timerError("Minute is not integer.");
		hour = 0;
		minute = 0;
		second = 0;
		return;
	}
	if (!Number.isInteger(second)) {
		timerError("Second is not integer.");
		hour = 0;
		minute = 0;
		second = 0;
		return;
	}


	if (second >= 60) {
		minute += Math.floor(second / 60);
		second = second % 60;
	}

	if (minute >= 60) {
		hour += Math.floor(minute / 60);
		minute = minute % 60;
	}

	if (hour > 99) {
		hour = 99;
	}

	timerError("");

	document.getElementById("timerMsg").innerHTML = "";

	document.getElementById("hourInput").value = hour;
	document.getElementById("minuteInput").value = minute;
	document.getElementById("secondInput").value = second;

	document.getElementById("hourOutput").innerHTML = twoDigit(hour);
	document.getElementById("minuteOutput").innerHTML = twoDigit(minute);
	document.getElementById("secondOutput").innerHTML = twoDigit(second);

}




function timerError(msg) {
	document.getElementById("timerErrorMsg").innerHTML = msg;
}



function startTimer(flag) {
	if (flag === '0') {
		//start timer
		if (hour === 0 && minute === 0 && second === 0) {
			return;
		}


		timer = setInterval(myTimer, 1000);

		document.getElementById("startStopBt").value = "Stop";
		document.getElementById("startStopBt").onclick = function() { startTimer('1') };

		document.getElementById("setTimerBt").disabled = true;
		document.getElementById("setTimerBt").style = "background-color: grey";

		document.getElementById("timerMsg").innerHTML = "";
	} else {
		//stop timer
		clearInterval(timer);
		document.getElementById("startStopBt").value = "Start";
		document.getElementById("startStopBt").onclick = function() { startTimer('0') };

		document.getElementById("setTimerBt").disabled = false;
		document.getElementById("setTimerBt").style = "background-color: black";
	}
}



function myTimer() {
	if (second === 0) {
		if (minute === 0) {
			if (hour !== 0) {
				second = 59;
				minute = 59;
				hour--;
			}
		} else {
			second = 59;
			minute--;
		}
	} else {
		second--;
	}

	document.getElementById("hourOutput").innerHTML = twoDigit(hour);
	document.getElementById("minuteOutput").innerHTML = twoDigit(minute);
	document.getElementById("secondOutput").innerHTML = twoDigit(second);

	if (second === 0 && minute === 0 && hour === 0) {
		//time's up
		clearInterval(timer);
		let alarm = document.getElementById("alarm");
		//alarm.volume = data["volume"] / 100;
		alarm.play();
		win.show();
		document.getElementById("startStopBt").value = "Start";
		document.getElementById("startStopBt").onclick = function() { startTimer('0') };

		document.getElementById("setTimerBt").disabled = false;
		document.getElementById("setTimerBt").style = "background-color: black";

		document.getElementById("timerMsg").innerHTML = "Time's up!";
	}
}



function twoDigit(s) {
	s = s.toString();
	while (s.length < 2) {
		s = '0' + s;
	}
	return s;
}




function readData() {
	try {
		if (fs.existsSync(dataFile)) {
			//file exists
			fs.readFile(dataFile, (err, jsonData) => {
				if (err) throw err;
				data = JSON.parse(jsonData);

				loadData();
				saveData();
			});
		} else {
			loadData();
			saveData();
		}
	} catch(err) {
		console.error(err);
	}
}


function loadData() {
	document.getElementById("volume").value = data["volume"];
	document.getElementById("volumeOutput").innerHTML = data["volume"].toString();


	win.setFullScreen(data["fullScreen"]);
	document.getElementById("fullScreenBt").value = (data["fullScreen"] ? "Exit full screen" : "Full screen");


	switch (data["playSongMethod"]) {
		case 0:
			document.getElementById("playSongLoop").checked = true;
			break;
		case 1:
			document.getElementById("playSongSequence").checked = true;
			break;
		case 2:
			document.getElementById("playSongRandom").checked = true;
			break;
	}


	loadSong();
}



function loadSong() {
	song = [];
	if (fs.existsSync(songFolder)) {
		fs.readdir(songFolder, (err, files) => {
			files.forEach(fileName => {
				if (fileName.slice(-4) === ".mp3") {
					song.push(fileName);
				}
				document.getElementById("songSelect").innerHTML = "<option value='' selected>---</option>";

				for (let i = 0; i < song.length; i++) {
					document.getElementById("songSelect").innerHTML += "<option value='" + song[i] + "'>" + song[i].slice(0, -4); + "</option>";
				}
			});
			document.getElementById("songInfo").innerHTML = " " + song.length.toString() + " song(s) are found.";
		});
	} else {
		//create songFolder
		fs.mkdirSync(songFolder);
		document.getElementById("songInfo").innerHTML = " 0 song(s) are found.";
	}


}




function changeSongName() {
	let newName = document.getElementById("newSongName").value;
	if (newName === "") {
		return;
	}
	let oldName = document.getElementById("songSelect").value;
	if (oldName === "") {
		return;
	}

	if (newName.slice(-4) !== ".mp3") {
		newName += ".mp3";
	}

	fs.rename(songFolder + oldName, songFolder + newName, () => {
		document.getElementById("renameMsg").innerHTML = "Please click Reload song button to see if it is renamed.";
		document.getElementById("newSongName").value = "";
	});
}



function removeSong() {
	let toRemove = document.getElementById("songSelect").value;
	if (toRemove === "") {
		return;
	}

	document.getElementById("songSelect").selectedIndex = 0;
	document.getElementById("songSelect").onchange();

	fs.unlink(songFolder + toRemove, function (err) {
		if (err) throw err;

		loadSong();
	});
}




function saveData() {
	let jsonData = JSON.stringify(data, null, 4);
	fs.writeFileSync(dataFile, jsonData);
}



function htmlEntities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}






function hideMusicDiv() {
	document.getElementById("musicDiv").className = "hide";
	document.getElementById("timerDiv").className = "show";

	document.getElementById("showMusicBt").value = "Music";
	document.getElementById("showMusicBt").onclick = function() { showMusicDiv() };
}

function showMusicDiv() {
	document.getElementById("musicDiv").className = "show";
	document.getElementById("timerDiv").className = "hide";

	document.getElementById("showMusicBt").value = "Timer";
	document.getElementById("showMusicBt").onclick = function() { hideMusicDiv() };
}


function playSong() {
	let songName = document.getElementById("songSelect").value;
	let song = document.getElementById("song");
	if (songName === "") {
		song.pause();
		song.currentTime = 0;
		song.innerHTML = "";
		return;
	}
	let volume = document.getElementById("volume").value;
	song.pause();
	song.currentTime = 0;
	song.innerHTML = "<source src='" + (__dirname.slice(-8) === "app.asar" ? '../../song/' : './song/') + songName + "' type='audio/mpeg'>";
	song.load();
	song.volume = volume / 100;
	document.getElementById("volumeOutput").innerHTML = volume.toString();
	song.play();
}


function playSongMethod(flag) {
	/*
	flag:
		0 ==> loop
		1 ==> sequence
		2 ==> random
	*/

	data["playSongMethod"] = parseInt(flag);


	saveData();
}


function stopSong() {
	let songSelect = document.getElementById("songSelect");
	songSelect.selectedIndex = 0;
	songSelect.onchange();
}


function changeVolume() {
	let song = document.getElementById("song");
	let volume = document.getElementById('volume').value;
	song.volume = volume / 100;
	document.getElementById("volumeOutput").innerHTML = volume.toString();

	data["volume"] = volume;
	saveData();
}
