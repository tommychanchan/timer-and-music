const {
	app,
	BrowserWindow
} = require('electron');

const path = require('path');

function createWindow() {
	win = new BrowserWindow({
		titleBarStyle: 'hidden',
		show: false,
		width: 1000,
		height: 800,
		icon: 'image/favicon.ico'
	});
	win.once('ready-to-show', () => {
		win.show();
	});
	win.loadFile('index.html');
	win.maximize();
	//win.webContents.openDevTools(); // Debug
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow();
	}
});

app.on('browser-window-created', (e,window) => {
	window.setMenu(null);
});
